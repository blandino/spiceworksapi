﻿using Microsoft.EntityFrameworkCore;
using Spiceworks_Web_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spiceworks_Web_Api.Data
{
    public class ApplicationDbContext: DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Kpi_Encuesta> Kpi_Encuesta { get; set; }
    }
}
