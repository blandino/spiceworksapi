﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Spiceworks_Web_Api.Data;
using Spiceworks_Web_Api.Models;

namespace Spiceworks_Web_Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetSpiceWorksDataController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        public GetSpiceWorksDataController(ApplicationDbContext context) 
        {
            _context = context;
        }

        //string connectionstring = "Driver = SQLite ODBC Driver; Datasource = C:/Users/wgarcia/source/repos/ConsoleApp3/ConsoleApp3/spiceworks_prod.db; Version = 3; New = True; Compress = True;";

        string fileRoute = @"C:/Users/afeliz/source/repos/spiceworksapi/ConnectionString.txt";
        string connectionstring;
        public string GetConneciontionString() {

            StreamReader st = new StreamReader(fileRoute);

            connectionstring = st.ReadLine();

            return connectionstring;
        }



        [HttpGet]
        public List<users> getAllUsers()
        {
            GetConneciontionString();

            SQLiteConnection con = new SQLiteConnection(connectionstring);
            con.Open();

            SQLiteCommand CMD = new SQLiteCommand("select * from users", con);
            CMD.CommandType = System.Data.CommandType.Text;
            var result = CMD.ExecuteReader();
            SQLiteDataReader read = result;

            List<users> usersList = new List<users>();
            usersList = DataReaderMapToList<users>(read);
            con.Close();

            return usersList;

        }

        [HttpPost]
        public IActionResult CreateKpi_Encuesta([FromBody] Kpi_Encuesta kpi_Encuesta)
        {
            _context.Kpi_Encuesta.Add(kpi_Encuesta);
            _context.SaveChanges();

            return Ok();
        }
        [Route(@"Get_TicketsReport/{from}/{to}")]
        [HttpGet]
        public List<Tickets> Get_TicketsReport(DateTime from, DateTime to) {
            GetConneciontionString();
            var lista = "";
            SQLiteConnection con = new SQLiteConnection(connectionstring);
            con.Open();
            SQLiteCommand CMD = new SQLiteCommand("select tickets.id,Summary,Status,tickets.created_at,closed_at,Users.first_name" +
                " ||' '|| Users.Last_Name Asigned_To,reopened,category,c_sucursal,c_departamento from " +
                "tickets left join users on Tickets.Assigned_to = users.id where tickets.created_at BETWEEN '" + from + "' and '" + to + "' ", con);
            CMD.CommandType = System.Data.CommandType.Text;
            var result = CMD.ExecuteReader();
            SQLiteDataReader read = result;

            List<Tickets> ticketList = new List<Tickets>();
            ticketList = DataReaderMapToList<Tickets>(read);
            con.Close();



            return ticketList;
        }

        public static List<T> DataReaderMapToList<T>(IDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (!object.Equals(dr[prop.Name], DBNull.Value))
                    {
                        if (prop.Name == "id" || prop.Name == "company_id" || prop.Name == "supervisor_id")
                        {
                            prop.SetValue(obj, dr[prop.Name].ToString(), null);

                        }
                        else
                        {
                            prop.SetValue(obj, dr[prop.Name], null);
                        }

                    }
                }
                list.Add(obj);
            }
            return list;
        }

        public class Tickets
        {
            public string id { get; set; }
            public string Summary { get; set; }
            public string Status { get; set; }
            public DateTime created_at { get; set; }
            public DateTime closed_at { get; set; }
            public string Asigned_To { get; set; }
            public bool reopened { get; set; }
            public string category { get; set; }
            public string c_sucursal { get; set; }
            public string c_departamento { get; set; }

        }

        public class users
        {
            public string id { get; set; }
            public string encrypted_password { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string email { get; set; }
            public string company_id { get; set; }

            public string display_name { get; set; }

            public bool verified { get; set; }
            public bool display_name_set { get; set; }

            public DateTime start_date { get; set; }
            public string department { get; set; }
            public string supervisor_id { get; set; }
        }

       /* public class Kpi_Encuesta
        {
            public int id { get; set; }
            public int UserId { get; set; }
            public string TicketNumber { get; set; }
            public int Tiempo { get; set; }
            public int Calidad { get; set; }
            public int Satisfaccion { get; set; }
        }*/

    }

}
