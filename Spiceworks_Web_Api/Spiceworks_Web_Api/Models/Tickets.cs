﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spiceworks_Web_Api.Models
{
    public class Tickets
    {
        public string id { get; set; }
        public string Summary { get; set; }
        public string Status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime closed_at { get; set; }
        public string Asigned_To { get; set; }
        public bool reopened { get; set; }
        public string category { get; set; }
        public string c_sucursal { get; set; }
        public string c_departamento { get; set; }
    }
}
