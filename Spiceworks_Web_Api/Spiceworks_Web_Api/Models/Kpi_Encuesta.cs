﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Spiceworks_Web_Api.Models
{
    public class Kpi_Encuesta
    {
        [Key]
        public int id { get; set; }
        public int UserId { get; set; }
        public string TicketNumber { get; set; }
        public int Tiempo { get; set; }
        public int Calidad { get; set; }
        public int Satisfaccion { get; set; }
    }
}
